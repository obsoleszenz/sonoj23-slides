import { html } from 'nox-html'
import { less } from './utils.js'

export class Template {
  path = ""
  title = ""

  script = `
      Reveal.initialize({
        plugins: [ RevealNotes ]
      });
  `
  style = less`
    :root {
      --r-heading-text-transform: normal !important;
    }

  `

  head = () => html`
    title: ${`obsoleszenz::${this.title}`}
    style: ${this.style}
    ${true && html`script(src='http://localhost:8123/nox-notify.js')` || ''}
    link(rel="stylesheet" href="reveal.js/reveal.css")
    link(rel="stylesheet" href="reveal.js/theme/white.css")
  `

  slides = ""

  skeleton = () => html`
    !DOCTYPE(html)
    html(xmlns="http://www.w3.org/1999/xhtml" lang="de" "xml:lang"="de") {
      head: ${this.head}
      body {
        .reveal:.slides: ${this.slides}
        script(src="reveal.js/reveal.js")
        script(src="reveal.js/plugin/notes/notes.js")
        script: ${this.script}
      }
    }
  `

  toString() {
    return html`${this.skeleton}`
  }
}
