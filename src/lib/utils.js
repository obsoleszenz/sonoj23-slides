import path from 'node:path'
import { fileURLToPath } from 'node:url'
import lessNode from 'less'

export function css(strings, ...values) {
  let concatenatedString = ''
  let pos = 0;
  for (let i = 0; i < strings.length; i++) {
    const string = strings[i];
    for (let j = 0; j < string.length; j++) {
      concatenatedString += string[j]
      pos++;
    }
    if (i < values.length) {
      concatenatedString += values[i]
      pos++;
    }
  }
  // Remove line breaks
  /*concatenatedString = concatenatedString.replace(/(\r\n|\n|\r)/gm, "")
  // Remove multiple white spaces
  concatenatedString = concatenatedString.replace(/ +(?= )/gm, "")
  // Trim
  concatenatedString = concatenatedString.trim()*/
  return concatenatedString
}

export function less(strings, ...values) {
  let concatenatedString = ''
  let pos = 0;
  for (let i = 0; i < strings.length; i++) {
    const string = strings[i];
    for (let j = 0; j < string.length; j++) {
      concatenatedString += string[j]
      pos++;
    }
    if (i < values.length) {
      concatenatedString += values[i]
      pos++;
    }
  }

  let renderedString = "" 
  lessNode.render(concatenatedString, {sync: true}, (err, result) => {
    if (err) throw err
    renderedString = result.css
  })

  // Remove line breaks
  renderedString = renderedString.replace(/(\r\n|\n|\r)/gm, "")
  // Remove multiple white spaces
  renderedString = renderedString.replace(/ +(?= )/gm, "")
  // Trim
  renderedString = renderedString.trim()
  return renderedString
}

export function __filename(importMeta) {
  return fileURLToPath(importMeta.url)
}

export function __dirname(importMeta) {
  return path.dirname(__filename(importMeta))
}
