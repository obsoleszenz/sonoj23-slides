import { html, less, Template } from './lib/prelude.js'

export const page = new Template()

const video = (src) => html`video(src=${src} autoplay loop)`

page.style += less`
  :root {
    --r-heading-text-transform: normal !important;
  }

  .absolute {
    position: absolute;
    z-index: 10;
  }
  .no-margin {
    margin: 0px 0px !important;
  }
  .reveal img {
    max-width: 60vw !important;
    max-height: 60vh !important;
  }
`

const ARROW = '→'

page.slides = () => html`
  section {
    img(src="assets/logo.svg" style="height: 10vh;margin-bottom:4vh")
    p: "Libre DJ & performance mixer"
    img.fragment(data-src="assets/cat-djing-meme.gif")
  }
  section {
    h2: "whoami"

    ul {
      li.fragment: "@obsoleszenz"
      li.fragment {
        img.absolute.no-margin(style="top: 0; right: 0vw" src="assets/obsoleszenz-sonoj.png")  
        "Techno DJ "
      } 
      li.fragment: "Tinkerer"
      li.fragment: "Exploring&Learning"
      li.fragment: "Means:"
      ul:li.fragment: "I don't know shit"
    }
  }
  section {
    img(src="assets/make-music-meme.jpg")
  }
  section {
    h2: "Links"
    ul {
      li: "codeberg.org/obsoleszenz"
      li: "soundcloud.com/obsoleszenz"
      li: "@obsoleszenz@nerdculture.de"
      li: "obsoleszenz@riseup.net"
  
    }
  }
  section {
    h2: "What is EQUIS?"
    ul {
      li.fragment: "DJ & Performance Mixer"
      li.fragment: "It's FOSS software"
      li.fragment: "Made for Linux, JACK & MIDI"
    }
  }
  section {
    h2: "What is a DJ Mixer?"
  }
  section {
    h2: "Xone:96"
    img(src="assets/xone96.jpg")
  }
  section {
    h2: "MODEL 1"
    img(src="assets/model1.jpg")
  }
  section {
    h2: 'Why build a Mixer?'
    ul {
      li.fragment: "Moneymoneymoney"
      li.fragment: "Existing solutions are lacking"
      li.fragment: "JACK & Linux is powerful"
      li.fragment: "DSP is very powerful too"
      li.fragment: "Exploring & Learning"
      li.fragment: "Built by me, for me"
      aside.notes {
        ul {
          li: "DJ & Music Gear is very expensive & silos"
          li: "Controllers & Software are often lacking behind Hardware Mixers"
          li: "Linux Pro Audio support is getting really good"
          li: "Custom Setups are cool, if everyone uses the same its boring"
          li: "I can build a setup fitting exactly my needs, portable & repairable"
        }
      }
    }
  }
  section {
    img(src="assets/building-gear-meme.jpg")
    aside.notes {
      ul:li:"TODO: Bogen zum anfang"
    }
  }
  section {
    h2: 'Why "DJ AND Performance" Mixer?'
    p.fragment: "Let's have a look at some Setups :)"
  }
  section {
    h2: "Setup of Richie Hawtin"
    img(src="assets/richie-hawtin-setup-1.png") 
  }
  section {
    h2: "Setup of Chris Liebing"
    img(src="assets/chris-liebing-setup-1.jpg")
  }
  section {
    h2: "Setup of Chris Liebing"
    img(src="assets/chris-liebing-setup-3.jpg")
  }
  section {
    h2: "Mixer == '♥'"
    ul {
      li.fragment: '„Heart” of the setup'
      li.fragment: 'Connecting DJ, Drum Machine, Effects, DAW...'
      li.fragment: "EQ/Filter/... to shape & modify the sound"
      li.fragment: "Tool & Instrument"
    }
  }
  section {
    h2: "Setup of meee"
    img(src="assets/obsoleszenz-setup-1.jpg")
  }
  section {
    h2: "Performance Time"
  }
  section {
    h2: "Concept of EQUIS?"
    ul {
      li.fragment: "Mixer as a Software"
      li.fragment: "Techno & Electronic Music focused"
      ul:li.fragment: "Layering DJing Style"
      ul:li.fragment: "Hybrid Setups"
      li.fragment: "MODEL1 like EQ"
      li.fragment: "Club ready"
      li.fragment: "Hackable"
    }
  }
  section {
    h2: "Screenshot"
    img(src="assets/screenshot1.png")
  }
  section {
    h2: "Screenshot"
    img(src="assets/screenshot-qpwgraph.png")
  }
  section {
    h2: "Features"
    ul {
      li.fragment: "8x Stereo Inputs"
      li.fragment: "2x Stereo Send"
      li.fragment: "2x SubFilters"
      li.fragment: "2x Headphone outputs"
      li.fragment: "1x Main Output"
      li.fragment: "MODEL1 like EQ"
    }
  }
  section {
    h2: "Let's talk EQ"  
    ul {
      li.fragment: "Core of a DJ Mixer"
      li.fragment: "No traditional 3 or 4 Band EQ"    
      li.fragment: "Combination of:"
      ul {
        li.fragment: "Highpass"
        li.fragment: "Lowpass"
        li.fragment: "Assymetric Bell EQ"
      }
    }
  }
  section {
    h2: "Let's talk CUE"  
    ul {
      li.fragment: "Prelistening"
      li.fragment: "Balance between CUE & Main"
      li.fragment: "Post/Pre EQ"
      li.fragment: "Limiter for Hearing Protection"
      li.fragment: "2x independent CUE Systems"
    }
  }
  section {
    h2: "Let's talk Send&Return"  
    ul {
      li.fragment: "It's for effects"
      li.fragment: "2x for double the fun!"
      li.fragment: "Control the amount of send"
      li.fragment: "Control the amount of return"
      li.fragment: "Return channels are normals channels"
    }
  }
  section {
    h2: "Let's talk SubFilters"  
    ul {
      li.fragment: "Route multiple channels through filter"
      li.fragment: "2x for double the fun!"
      li.fragment: "Many Controls, Versatile & Character"
      li.fragment: "Cool for drops & mixing"
      li.fragment: "Inspired by Xone"
    }
  }
  section {
    h2: "Compatible Software"
    ul {
      li.fragment: 'Mixxx'
      li.fragment: "xwax"
      li.fragment: "librecdj"
      li.fragment: "External sources like cdjs/vinyl/guitar pedals"
      li.fragment: "→ Everything JACK"
      li.fragment: "Recommendation: Cardinal, Dragonfly, Jalv/Carla"
    }
  }
  section {
    h2: "Compatible Controllers"
    ul {
      li.fragment: 'Now only mine :/'
      li.fragment: "Support for midi mappings"
      li.fragment: "Hackable!"
      li.fragment: "Off the shelf suitable hardware?"
      li.fragment: "Open Hardware controllers?"
    }
  }
  section {
    h2: "Perspective"
    ul {
      li: "1.0"
      ul {
        li: "Finalize EQ"
        li: "Finalize SubFilters"
        li: "Overdrive?"
        li: "Selectable fader curves?"
        li: "Main EQ?"
        li: "Booth Output!"
      }
    }
  }
  section {
    h2: "Thanks"
    ul {
      li.fragment: "Anna, Muni, Tommy & Photonis"
      li.fragment: "FAUST"
      li.fragment: "Mixxx/Moiré"
      li.fragment: "Richie Hawtin & Andy Rigby-Jones for the inspiration"
    }
  }
  section {
    img(src="assets/building-gear-meme.jpg")
    aside.notes {
      ul:li:"TODO: Bogen zum anfang"
    }
  }

`

page.slides2 = () => html`






`
