# Ideen
- Who am I?
  - DJing Techno
  - Tinkering with Electronics/Hardware/Design/System Programming
- Richie Hawtin und sein Engineer als kleine Introduction
  - Innovations in DJing, the Club Mixer 
  - Richies Style of DJing
    - The magic third deck
    - Incorporating Drum machines/synthesizers
    - => Performance Mixer, Mixer as an Instrument
- A Mixer that feels like an Instrument
  - Playable, quick to fix things
  - Filterdesign & eq design are very important
    - Filters steep enough to cut/reduce & recombine elements
    - Not too steep, otherwise they are resonant
    - Good frequency & db ranges 
      - Too small and they limit
      - Too Big and the potentiometer ranges are too fine
  - Creativity
    - Enough Inputs & Outputs to do weird things with
- Idea of this talk
  - Inspire out of the Box DJing
  - Public Showoff of EQUIS
  - Maybe we can build a Community of like minded Minds?
- Demo
  - A little bit like Crossfader/DJ Tutorials on YouTube
    - How I Play    
  - HPF/LPF
    * Cutting away/down to Kick/Bassline/Vocals/Hihats
      * A Track with a Boring Kick
      * Spicing up the Groove
      * Adding/Replacing HiHats
  - BellEQ
    * Playing with the Dynamic of Elements
    * Mixing Kick/Bassline/Vocals/HighHats
  - Send&Return Effects
    * Using Cardinal for simulating Guitar Pedals
    * Reverb
    * Delay
    * Carla as Plugin Host
  - Drum Machine??