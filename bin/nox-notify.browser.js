function docReady(fn) {
  // see if DOM is already available
  if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 1)
  } else {
      document.addEventListener("DOMContentLoaded", fn)
  }
}    

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}


function parse_state() {
  let location = decodeURI(document.location.toString())
  let state = {}
  let state_slice = '#state='
  let state_offset = location.indexOf(state_slice)
  if (state_offset !== -1) {
    let state_str = location.slice(state_offset + state_slice.length, location.length)
    console.log(state_str)
    state = JSON.parse(state_str)
    location = location.slice(0, state_offset)
  }

  return {location, state}
  
}


function storeState() {
    let { location } = parse_state()
    let state = JSON.stringify(Reveal.getState())
    window.location.replace(location + '#state=' + state)
}

docReady(async () => {
  Reveal.on('slidechanged', storeState);
  Reveal.on('fragmentshown', storeState);
  Reveal.on('fragmenthidden', storeState);
  
  {
    // Restore state
    let restore = parse_state()  
    if (restore.state != {}) {
      console.log('Restoring state:', JSON.stringify(restore.state))
      Reveal.setState(restore.state)
    }
  }


  
  let was_offline = false
  while (true) {
    try {
      const status = await fetch('http://localhost:8123/api/status')
      console.log('[NotifierServer] Connected')

      if (was_offline) {
        window.location.reload()
        return
      }

      const status_json = await status.json()
      console.debug(status_json)
      if (status_json.status === 'fails') {

        document.body.innerHTML = '<pre>' + status_json.data + '</pre>'
      }



      while (true) {
        const response = await Promise.race([
          fetch('http://localhost:8123/api/pollEvent'),
        ])
        const response_json = await response.json()
        console.log('on watch event', response_json)

        if (response_json.type === 'filesystem_change' || response_json.type === 'status') {
          console.log("reloooad")
          storeState()
          window.location.reload(true)
        }
      }
    } catch (err) {
      console.log(`[err] ${err}`)
      await sleep(1000)
      was_offline = true
    }
  }
})
