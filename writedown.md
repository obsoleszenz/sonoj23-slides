# Ablauf
- Quick Introduction
  - So that people "sonically & visually" understand the performance
  - What is EQUIS?
    - Show pictures of mixers (DJM 900NXS2, Xone:92, MODEL1)
    - Show pictures of setups
- Performance ~6min

------

- My Journey
  - I started djing with blubb
  - Inspiration
  - First controller
  - First diy controller...
  - First hardware mixer
    - Problems with it
  - Second hardware mixer
    - Problems with it
  - => Why I want to DIY
- Feature Dive
  - Difference to just using Ardour/Bitwig as a mixer
    - "DAWless"/more modular
    - Built specific for Performance Mixing
  - EQ
  - Send/Return for effects
  - CUE System for prelistening
- Compatibility
  - With other Software
  - With Hardware
    - External Audio Sources
    - MIDI Controllers
- Hackability
  - Building a MIDI Mapping
  - FAUST
- Concept
  - It's an instrument
    - It's fine to be limited and not add features
      - No featuritis
      - It's an exploration for now, concept freeze with 1.0
      - We can always to an EQUIS-2
    - It's fine to be niche
    - Please have character
    - Please have concept
    - Sound characteristic should only change with major versions
  - No panic in the disquothek!!
  - I would love to have hardware specific to this, but for now I only have my prototypes, no PCB designs
- Explain exploration phase
  - Decide on final EQ design
  - Decide on final SubFilter design
  - There are many ideas for features
    - Compressor/Saturator/Overdrive to circumvent digital distortion
    - Main Isolator
    - Booth output
    - Pre Fader send
- More performances
  - Sprinkle in between of the talk?
  - Replacing low end
  - Layering low end
  - Layering tracks
  - Creating tension


# What is EQUIS?


EQUIS is an exploration of a software performance mixer for blending & manipulation of multiple musical pieces, instruments and effects. The target usage is DJ and Live Sets, For the sound & offered features it takes inspiration from mixers like the Allen&Heath Xone:92 and the  PlayDifferently MODEL1.

Exploration takes place around a new style of EQing. This style uses a low & highpass combined with a sweepable bell eq. The PlayDifferently MODEL1 for example offers this kind of EQ. I don't know of any open source 



-----------

So obsoleszenz, what is it, this equis thingy? Good that you ask amigo!
It's nothing more then a software mixer, meant for djing/live sets and likewise. It mixes up to 8 stereo channels, allows you to do some nice eqing and filtering, offers two send & return channels for external effects and not one but two decent CUE system so that you can prelisten individual and mixed channels on your headphones.  

# And how can I use it?

Well, you need Linux, and a midi controller. Then, through JACK you can plumb whatever audio applications or sources you want through this software and you get the mixed signalout of it. And those you can then output through your audio interface.

As we use JACK as the audio backend, most audio sources are supported. From classic DJ programs like Mixxx or xwax, VST plugins, drum machines, synthesizers, DAWs like Bitwig but also, if your audio interface supports it, external sources like vinyls, CDJs, guitar pedals and so. And as equis supports 8 stereo inputs, you can also mix multiple of those sources, doing hybrid setups with eg 2 Vinyl players, CDJs, a drum computer for the extra spice and an VST effect plugin to glue everything together. 

# And how can I control it?

In theory with every MIDI Controller that offers the controls you want. EQUIS offers an interface where you can add your own mapping with a little code. It's designed in a way to offer the lowest possible latency. Have a look at the readme on the github. Also I hope that soon we will have support for some off the shelf midi controllers. 

# How does your setup look like?

I'm mainly DJing, so I'm using a DJ Program, think Mixxx and two MIDI controllers that I built for this purpose. Also I'm using Dragonfly Reverb and a Cardinal Delay Patch as effects. This is how it looks like. As an Audio Interface im currently using the gigaport hd+ and a good old thinkpad x230 all running on ArchLinux with PipeWire-JACK. 

For jamming i also like to wire up my Roland TR8-S to it, to play some drums on top.



